/**
 * @file
 * @author The CS2 TA Team <cs2-tas@ugcs.caltech.edu>
 * @date 2014
 * @copyright This code is in the public domain.
 * 
 * @brief Another array example with pointers.
 */

#include <cstdio>
#include <cstdlib>
#include <ctime>

#define TEST_SIZE 60

//
// TODO: put user functions here
//

/**
 * @brief Sets up and runs an array example.
 */
int main(int argc, char ** argv)
{
    
    /*-------- CHANGE NOTHING BELOW THIS LINE --------*/
    int * test_values;
    int real_size;
    
    // seed the PRNG
    srand(time(NULL));
    
    // allocate the array
    test_values = new int[TEST_SIZE];
    
    // initialize the test array to garbage
    for(int i = 0; i < TEST_SIZE; i++)
    {
        *(test_values+i) = rand();
    }
    
    // determine a real size
    real_size = TEST_SIZE - (rand() % 20);
    
    // initialize the meaningful part of the test array to random numbers
    // all of which are less than one million
    for(int i = 0; i < real_size; i++)
    {
        *(test_values+i) = rand() % 1000000;
    }
    /*-------- CHANGE NOTHING ABOVE THIS LINE --------*/
    
    //
    // TODO: do your stuff here
    //
}
